#ifndef BLFL2_UTILS_HPP
#define BLFL2_UTILS_HPP

#include <Config.hpp>
#include <Macros.hpp>
#include <cassert>

#include <stdint.h>

/**
 * Common methods and interfaces.
 */

extern "C" void __cxa_pure_virtual();

extern "C" void print_backtrace(const char* fmt, ...);

#ifndef NATIVE
    void print_backtrace(const char* fmt, ...) {
        /* NOP when not running is a hosting OS. */
    }
#endif

class INonCopyable {
   public:
    INonCopyable& operator=(const INonCopyable&) = delete;
    INonCopyable(const INonCopyable&) = delete;
    INonCopyable() = default;
};

struct pack int24_t {
   protected:
    unsigned char _d[3];

   public:
    int24_t() {}
    int24_t(const int val) {
        *this = val;
    }
    int24_t(const int24_t& val) {
        *this = val;
    }

    operator int() const {
        int r = 0;
        __builtin_memcpy(&r, _d, sizeof(_d));
        // Sign extend.
        if (r & 0x800000) {
            // Negative.
            r |= 0xff000000;
        }
        return r;
    }
    operator float() const {
        return (float) this->operator int();
    }

    int24_t& operator=(const int24_t& input) {
        __builtin_memcpy(this->_d, input._d, sizeof(_d));
        return *this;
    }
    int24_t& operator=(const int input) {
        __builtin_memcpy(this->_d, &input, sizeof(_d));
        return *this;
    }

    /***********************************************/

    int24_t operator+(const int24_t& val) const {
        return int24_t((int) *this + (int) val);
    }

    int24_t operator-(const int24_t& val) const {
        return int24_t((int) *this - (int) val);
    }

    int24_t operator*(const int24_t& val) const {
        return int24_t((int) *this * (int) val);
    }

    int24_t operator/(const int24_t& val) const {
        return int24_t((int) *this / (int) val);
    }

    /***********************************************/

    int24_t operator+(const int val) const {
        return int24_t((int) *this + val);
    }

    int24_t operator-(const int val) const {
        return int24_t((int) *this - val);
    }

    int24_t operator*(const int val) const {
        return int24_t((int) *this * val);
    }

    int24_t operator/(const int val) const {
        return int24_t((int) *this / val);
    }

    /***********************************************/

    int24_t& operator+=(const int24_t& val) {
        *this = *this + val;
        return *this;
    }

    int24_t& operator-=(const int24_t& val) {
        *this = *this - val;
        return *this;
    }

    int24_t& operator*=(const int24_t& val) {
        *this = *this * val;
        return *this;
    }

    int24_t& operator/=(const int24_t& val) {
        *this = *this / val;
        return *this;
    }

    /***********************************************/

    int24_t& operator+=(const int val) {
        *this = *this + val;
        return *this;
    }

    int24_t& operator-=(const int val) {
        *this = *this - val;
        return *this;
    }

    int24_t& operator*=(const int val) {
        *this = *this * val;
        return *this;
    }

    int24_t& operator/=(const int val) {
        *this = *this / val;
        return *this;
    }

    /***********************************************/

    int24_t operator>>(const int val) const {
        return int24_t((int) *this >> val);
    }

    int24_t operator<<(const int val) const {
        return int24_t((int) *this << val);
    }

    /***********************************************/

    int24_t& operator>>=(const int val) {
        *this = *this >> val;
        return *this;
    }

    int24_t& operator<<=(const int val) {
        *this = *this << val;
        return *this;
    }

    /***********************************************/

    operator bool() const {
        return (int) *this != 0;
    }

    bool operator!() const {
        return !((int) *this);
    }

    int24_t operator-() {
        return int24_t(-(int) *this);
    }

    /***********************************************/

    bool operator==(const int24_t& val) const {
        return (int) *this == (int) val;
    }

    bool operator!=(const int24_t& val) const {
        return (int) *this != (int) val;
    }

    bool operator>=(const int24_t& val) const {
        return (int) *this >= (int) val;
    }

    bool operator<=(const int24_t& val) const {
        return (int) *this <= (int) val;
    }

    bool operator>(const int24_t& val) const {
        return (int) *this > (int) val;
    }

    bool operator<(const int24_t& val) const {
        return (int) *this < (int) val;
    }

    /***********************************************/

    bool operator==(const int val) const {
        return (int) *this == val;
    }

    bool operator!=(const int val) const {
        return (int) *this != val;
    }

    bool operator>=(const int val) const {
        return (int) *this >= val;
    }

    bool operator<=(const int val) const {
        return (int) *this <= val;
    }

    bool operator>(const int val) const {
        return ((int) *this) > val;
    }

    bool operator<(const int val) const {
        return (int) *this < val;
    }
};

static_assert(sizeof(int24_t) == 3, "int24_t is actually not 24 bits!");

namespace blf {

inline uint32_t float2int(float v) {
    uint32_t r;
    __builtin_memcpy(&r, &v, sizeof(r));
    return r;
}

inline uint64_t float2int(double v) {
    uint64_t r;
    __builtin_memcpy(&r, &v, sizeof(r));
    return r;
}

template<class T> constexpr const T& clamp(const T& v, const T& lo, const T& hi) {
    assert(!(hi < lo));
    return (v < lo) ? lo : (hi < v) ? hi : v;
}

}  // namespace blf

#endif /*BLFL2_UTILS_HPP*/