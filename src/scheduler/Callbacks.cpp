#include <Macros.hpp>

#include <FreeRTOS.h>
#include <stdio.h>
#include <task.h>

C_DECLS_START
void vApplicationIdleHook(void);
void vApplicationStackOverflowHook(TaskHandle_t xTask, char* pcTaskName);
C_DECLS_END

void vApplicationIdleHook(void) {
    puts("FreeRTOS::Idle");
}

void vApplicationStackOverflowHook(TaskHandle_t xTask, char* pcTaskName) {
    trap();
}