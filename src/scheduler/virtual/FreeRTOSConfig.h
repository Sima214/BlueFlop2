#ifndef BLFL2_FREERTOSCONFIG
#define BLFL2_FREERTOSCONFIG

#include <Macros.hpp>

#include <FreeRTOSCommonConfig.h>
#include <stdio.h>

void vAssertCalled(char* file, int line);

#define configTICK_RATE_HZ 100
#define configAPPLICATION_ALLOCATED_HEAP 1
#define configTOTAL_HEAP_SIZE 8 * 1024 * 1024

#define configASSERT(x)                                                                        \
    if ((x) == 0) {                                                                            \
        vAssertCalled(__FILE__, __LINE__);                                                     \
    }

#endif /*BLFL2_FREERTOSCONFIG*/
