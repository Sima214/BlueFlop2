#ifndef BLFL2_ETL_PROFILE
#define BLFL2_ETL_PROFILE

#define ETL_NO_STL
#define ETL_LOG_ERRORS

// Debug oriented
#define ETL_VERBOSE_ERRORS
#define ETL_CHECK_PUSH_POP

#endif /*BLFL2_ETL_PROFILE*/