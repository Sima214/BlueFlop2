#include "Utils.hpp"

#include <Config.hpp>
#include <Macros.hpp>

#include <execinfo.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

void __cxa_pure_virtual() {
    trap();
}

#ifdef NATIVE

void print_backtrace(const char* fmt, ...) {
    if (fmt != NULL) {
        va_list args;
        va_start(args, fmt);
        vprintf(fmt, args);
        va_end(args);
        puts(":");
    }
    void* stackframes[24];
    int depth = backtrace(stackframes, sizeof(stackframes) / sizeof(void*));
    char** traces = backtrace_symbols(stackframes, depth);
    for (int i = 1; i < depth; i++) {
        printf("  %d. %s.\n", i, traces[i]);
    }
    free(traces);
}

#endif