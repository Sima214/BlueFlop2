#ifndef C_DECLS_START
  #ifdef __cplusplus
    /**
     * Should be used before the inclusion of a C header file in C++ source code.
     */
    #define C_DECLS_START extern "C" {
  #else
    /**
     * Should be used before the inclusion of a C header file in C++ source code.
     */
    #define C_DECLS_START
  #endif
#endif

#ifndef C_DECLS_END
  #ifdef __cplusplus
    /**
     * Should be used after the inclusion of a C header file in C++ source code.
     */
    #define C_DECLS_END }
  #else
    /**
     * Should be used after the inclusion of a C header file in C++ source code.
     */
    #define C_DECLS_END
  #endif
#endif

#ifndef nop
  /**
   * Generates a single nop instruction.
   */
  #define nop __asm__("nop")
#endif

#ifndef unreachable
  /**
   * Expression that will never be executed.
   * Only changes optimization behaviour.
   */
  #define unreachable __builtin_unreachable
#endif

#ifndef trap
  #if defined(__arm__)
    /**
     * Generates an invalid instruction.
     */
    #define trap __asm__("udf 0x0");unreachable
  #elif defined(__x86_64__) || defined(__i386__)
    /**
     * Generates an invalid instruction.
     */
    #define trap __builtin_trap();unreachable
  #endif
#endif

#ifndef noinline
  /**
   * Seems to work even when lto is enabled.
   */
  #define noinline __attribute__((noinline))
#endif

#ifndef interrupt
  /**
   * Marks a function as an interrupt service routine.
   */
  #define interrupt __attribute__((interrupt))
#endif

#ifndef naked_interrupt
  /**
   * Marks a function as an interrupt service routine.
   */
  #define naked_interrupt __attribute__((naked))
#endif

#ifndef mask_create
  /**
   * Generates a mask with the bit-th bit turned on.
   */
  #define mask_create(bit) (0x1ll << (bit))
#endif

#ifndef mask_test
  /**
   * Tests if all the bits of the mask are turned on in the input.
   */
  #define mask_test(o, m) (!!(o & m))
#endif

#ifndef mask_set
  /**
   * Sets or unsets bits based on the mask according to the boolean
   * value v. The value of the first parameter is overwritten.
   */
  #define mask_set(o, m, v) o = ((~m) & o) | (v ? m : 0x0)
#endif

#ifndef mask_toogle
  /**
   * Toogles all the bits of the first parameter based on the mask.
   */
  #define mask_toogle(o, m) o ^= m
#endif

#ifndef extract_byte
  /**
   * Gets the \p i th byte of value.
   */
  #define extract_byte(v, i) (v >> (i * __CHAR_BIT__)) & 0xff;
#endif

#ifndef hot_branch
  /**
   * Marks a branch as likely.
   * Use only when compiler is being stupid.
   */
  #define hot_branch(cond) __builtin_expect(cond, 1)
#endif

#ifndef cold_branch
  /**
   * Marks a branch as unlikely.
   * Use only when compiler is being stupid.
   */
  #define cold_branch(cond) __builtin_expect(cond, 0)
#endif

#ifndef unused
  /**
   * Use it when something is not getting used,
   * you know about it,
   * but you just want the compiler to shut up.
   */
  #define unused __attribute__((unused))
#endif

#ifndef pack
  #define pack __attribute__((__packed__))
#endif

#ifndef nonnull
  #define nonnull(...) __attribute__((nonnull(__VA_ARGS__)))
#endif

#ifndef strequal
  /**
   * Returns non zero if the contents of s1 equal the contents of s2.
   * You need to also include <string.h>
   */
  #define strequal(s1, s2) (strcmp(s1, s2) == 0)
#endif

#ifndef kibytes
  #define kibytes(a) (a*1024)
#endif

#ifndef mibytes
  #define mibytes(a) (kibytes(a)*1024)
#endif
