#ifndef BLFL2_BASE_HPP
#define BLFL2_BASE_HPP

#include <etl/array_view.h>
#include <etl/unordered_set.h>
#include <etl/vector.h>
#include <stddef.h>
#include <stdint.h>

namespace blf {

class IClockProvider {
   public:
    IClockProvider() = default;
    virtual ~IClockProvider() = default;
    /**
     * Gets the clock rate in Hz.
     * If 0, then clock is variable,
     * like the CPUs of a modern systems.
     */
    virtual uint32_t get_clock_rate() = 0;
};

class IClockable {
   private:
    IClockProvider* _source;

   protected:
    /**
     * Implemented for each module.
     *
     * @param old_clock_source The clock source that is getting changed,
     * or nullptr if the module was not previously getting clocked.
     * @param new_clock_source The clock source to change to.
     * @returns If the clock source change is valid.
     */
    virtual bool on_clock_source_change(IClockProvider* old_clock_source,
                                        IClockProvider* new_clock_source) = 0;

   public:
    IClockable() = default;
    virtual ~IClockable() = default;
    /**
     * Sets a new clock source. Returns the old clock source(or nullptr).
     */
    IClockProvider* set_clock_source(IClockProvider* clock_source);
    /**
     * Gets the active clock source, or nullptr if this module is not getting clocked.
     */
    IClockProvider* get_clock_source();
};

/**
 * Multiple (static) clock sources.
 */
template<size_t CLOCK_COUNT> class IMultiClockable : public IClockable {
   private:
    const etl::array_view<IClockProvider*> _clock_providers;

   public:
    constexpr IMultiClockable(const IClockProvider* ck[CLOCK_COUNT]) :
        _clock_providers(ck) {}

    virtual ~IMultiClockable() = default;
};

class IClockDivider : public IClockProvider, public IClockable {
   public:
    IClockDivider() = default;
    virtual ~IClockDivider() = default;
    /**
     * Get clock division factor (read for example from a peripheral register).
     */
    virtual uint32_t get_division_factor() = 0;

    /**
     * Overrides.
     */
    virtual uint32_t get_clock_rate() override {
        auto input_clock = get_clock_source();
        if (input_clock != nullptr) {
            uint32_t factor = get_division_factor();
            uint32_t input_clock_rate = input_clock->get_clock_rate();
            uint32_t output_clock_rate = input_clock_rate / factor;
            return output_clock_rate;
        }
        return 0;
    }
};

template<typename CHILD_TYPE, size_t CHILD_COUNT> class IParent {
   private:
    const etl::vector<CHILD_TYPE*, CHILD_COUNT> _children;

   protected:
    /**
     * Called for every new child.
     *
     * Return true accept.
     */
    virtual bool on_new_child(CHILD_TYPE&) = 0;
    /**
     * Called for every child that is to be removed.
     *
     * Return true accept removal.
     */
    virtual bool on_remove_child(CHILD_TYPE&) = 0;

   public:
    /**
     * Adds a new child.
     *
     * Returns true on success.
     */
    bool child_add(CHILD_TYPE& o) {
        if (!_children.full()) {
            bool parent_accepts = on_new_child(o);
            if (parent_accepts) {
                bool child_accepted = o.parent_add(*this);
                if (child_accepted) {
                    _children.push_back(&o);
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Remove a registered child at the specified iterator position.
     * Iterator is assumed to be valid.
     *
     * Returns true on success.
     */
    CHILD_TYPE* child_remove(CHILD_TYPE** child_index) {
        CHILD_TYPE& o = **child_index;
        bool parent_rejects = on_remove_child(o);
        if (parent_rejects) {
            bool child_rejected = o.parent_remove(*this);
            if (child_rejected) {
                _children.erase(child_index);
            }
        }
        return false;
    }
    /**
     * Remove a registered child object.
     *
     * Returns true on success.
     */
    bool child_remove(CHILD_TYPE& o) {
        auto child_index = etl::find(_children.begin(), _children.end(), o);
        bool child_registered = child_index != _children.end();
        if (child_registered) {
            return child_remove(child_index);
        }
        return false;
    }
    /**
     * Remove a registered child at the specified index.
     *
     * Returns true on success.
     */
    bool child_remove(size_t index) {
        if (index < _children.size()) {
            auto child_index = _children.begin() + index;
            return child_remove(child_index);
        }
        return false;
    }

    /**
     * Returns the reference to the value stored at the passed iterator position.
     *
     * A cat may die if iterator is invalid.
     */
    CHILD_TYPE& child_get(CHILD_TYPE** index) {
        return *index;
    }
    /**
     * Returns the reference to the value stored at the specified index.
     *
     * Etl asserts if index is invalid.
     */
    CHILD_TYPE& child_get(size_t index) {
        return _children.at(index);
    }

    /**
     * Returns the count of currently registered children.
     */
    size_t children_count() {
        return _children.size();
    }
    auto children_begin() {
        return _children.begin();
    }
    auto children_end() {
        return _children.end();
    }
};

template<typename PARENT_TYPE, size_t PARENT_COUNT> class IChild {
   private:
    const etl::unordered_set<PARENT_TYPE*, PARENT_COUNT> _parents;

   protected:
    /**
     * Called for every new parent.
     *
     * Return true to accept.
     */
    virtual bool on_new_parent(PARENT_TYPE&) = 0;
    /**
     * Called for every parent that is to be removed.
     *
     * Return true to accept removal.
     */
    virtual bool on_delete_parent(PARENT_TYPE&) = 0;

   public:
    /**
     * Register a new parent.
     *
     * Returns true on success.
     */
    bool parent_add(PARENT_TYPE& p) {
        if (!_parents.full() && !contains_parent(p) && on_new_parent(p)) {
            _parents.insert(&p);
            return true;
        }
        return false;
    }
    /**
     * Removes one previously registered parent.
     *
     * Returns true on success.
     */
    bool parent_remove(PARENT_TYPE& p) {
        if (on_delete_parent(p)) {
            size_t removed_count = _parents.erase(&p);
            return removed_count != 0;
        }
        return false;
    }

    /**
     * Returns true if parent is registered.
     */
    bool contains_parent(PARENT_TYPE& p) {
        return _parents.find(&p) != _parents.end();
    }
    /**
     * Read-only iterators.
     */
    auto parents_begin() {
        return _parents.begin();
    }
    auto parents_end() {
        return _parents.end();
    }
};

// Explicit specializations for single parent and/or child families.

template<typename CHILD_TYPE> class IParent<CHILD_TYPE, 1> {
   private:
    CHILD_TYPE* _child = nullptr;

   protected:
    virtual bool on_new_child(CHILD_TYPE&) = 0;
    virtual bool on_remove_child(CHILD_TYPE&) = 0;

   public:
    bool child_add(CHILD_TYPE& o) {
        if (_child == nullptr) {
            bool parent_accepts = on_new_child(o);
            if (parent_accepts) {
                bool child_accepted = o.parent_add(*this);
                if (child_accepted) {
                    _child = &o;
                    return true;
                }
            }
        }
        return false;
    }

    bool child_remove() {
        if (_child != nullptr) {
            bool parent_rejects = on_remove_child(*_child);
            if (parent_rejects) {
                bool child_rejects = _child->parent_remove(*this);
                if (child_rejects) {
                    _child = nullptr;
                    return true;
                }
            }
        }
        return false;
    }
    bool child_remove(CHILD_TYPE& o) {
        if (_child != nullptr && o == *_child) {
            return child_remove();
        }
        return false;
    }

    size_t children_count() {
        return _child != nullptr ? 1 : 0;
    }
};

template<typename PARENT_TYPE> class IChild<PARENT_TYPE, 1> {
   private:
    PARENT_TYPE* _parent;

   protected:
    virtual bool on_new_parent(PARENT_TYPE&) = 0;
    virtual bool on_delete_parent(PARENT_TYPE&) = 0;

   public:
    bool parent_add(PARENT_TYPE& p) {
        if (_parent == nullptr && on_new_parent(p)) {
            _parent = &p;
            return true;
        }
        return false;
    }

    bool parent_remove() {
        if (_parent != nullptr && on_delete_parent(*_parent)) {
            _parent = nullptr;
            return true;
        }
        return false;
    }
    bool parent_remove(PARENT_TYPE& p) {
        if (_parent != nullptr && p == *_parent) {
            return parent_remove();
        }
        return false;
    }

    /**
     * Returns true if parent is registered.
     */
    bool contains_parent(PARENT_TYPE& p) {
        return _parent != nullptr && *_parent == p;
    }
};

}  // namespace blf

#endif /*BLFL2_BASE_HPP*/