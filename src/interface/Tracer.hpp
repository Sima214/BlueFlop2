#ifndef BLFL2_TRACER_HPP
#define BLFL2_TRACER_HPP

#include <Macros.hpp>
#include <Utils.hpp>

#include <stddef.h>
#include <stdint.h>

namespace blf {

struct pack Trace {

    enum TraceSources {
        GLOBAL = 1,
        LIBC,
        ETL,
        FREERTOS,
        LIBOPENCM,
        CORE,
        BOARD,
        MODULE,
        SCHEDULER,
        COMMUNICATOR,
        LINKER,
    };

    enum CommonTypes {
        NO_MEMORY = 1,
        UNINITIALIZED,
        INTEGER_OVERFLOW,
        STACK_OVERFLOW,
        BUFFER_OVERFLOW,
        INDEX_OUT_OF_BOUNDS,
        __COMMON_TYPES_END
    };
    enum CommunicatorTypes { INVALID_TOKEN = __COMMON_TYPES_END, ALREADY_PARSED, ILL_FORMED };

    struct pack Compact {
        /**
         * False if no error happened.
         */
        bool error : 1;
        /**
         * Component of trace origin.
         */
        unsigned int source : 15;
        /**
         * Type of error.
         */
        unsigned int type : 16;

        /**
         * Full constructor.
         */
        template<typename TE>
        Compact(bool error, TraceSources src, TE type) : error(error), source(src), type(type) {
            if (error) {
                print_backtrace("Trace::Compact(%s, %s)", source2str(src), type2str(src, type));
            }
        }
        /**
         * Constructor for errors.
         */
        template<typename TE> Compact(TraceSources src, TE type) : Compact(true, src, type) {}
        /**
         * No error.
         */
        Compact() : Compact(false, (TraceSources) 0, 0) {}

        operator bool() const {
            return error;
        }
    };

   private:
    /**
     * ID of originating device.
     */
    unsigned int device : 15;
    /**
     * True if entry is valid.
     */
    bool present : 1;
    /**
     * 48 bits representing milliseconds since the Unix epoch.
     * 0 if time was not available at creating.
     */
    long long timestamp : 48;
    /**
     * True if reporting trace was also an error.
     */
    bool fatal : 1;
    /**
     * Component of trace origin.
     */
    unsigned int source : 15;
    /**
     * Type of error.
     */
    unsigned int type : 16;
    /**
     * Program counter.
     */
    void* pc;
    /**
     * Stack pointer.
     */
    void* sp;
    /**
     * Context specific data.
     */
    void* details[3];

   public:
    Trace() : present(0) {}
    Trace(TraceSources src) : present(1), source(src) {}

    /**
     * Get storage entry size in bytes.
     */
    constexpr size_t get_entry_size() {
        return 12 + 5 * sizeof(void*);
    }

   protected:
    static const char* source2str(TraceSources src);

    static const char* type2str(TraceSources src, int type);
};

/**
 * An interface for device/file where coredumps and traces are stored.
 */
class Tracer : private INonCopyable {
   public:
    Tracer() = default;
    virtual ~Tracer() = default;
    /**
     * Log a trace event.
     */
    virtual void trace(Trace&) = 0;
    /**
     * Dump core state.
     */
    virtual void coredump() = 0;
};

/**
 * /dev/null style trace.
 */
class NullTracer : public Tracer {
   public:
    NullTracer() = default;
    virtual ~NullTracer() = default;
    /**
     * Overrides.
     */
    virtual void trace(Trace&) override;
    virtual void coredump() override;
};

}  // namespace blf

#endif /*BLFL2_TRACER_HPP*/