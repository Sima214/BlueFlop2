#include "Core.hpp"

#include <Macros.hpp>

#include <stdio.h>

namespace blf {

void VirtualCore::init() {
    puts("Core::init");
    Core::init();
}

void VirtualCore::reset() {
    puts("Core::reset");
    trap();
}

}  // namespace blf