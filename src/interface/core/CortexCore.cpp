#include "Core.hpp"

#include <Config.hpp>
#include <Macros.hpp>

#include <libopencm3/cm3/scb.h>

namespace blf {

void CortexCore::init() {
    if constexpr (CPU_HAS_FPU) {
        // Enable access to Floating-Point coprocessor.
        SCB_CPACR |= SCB_CPACR_FULL * (SCB_CPACR_CP10 | SCB_CPACR_CP11);
    }
    Core::init();
}

void CortexCore::reset() {
    trap();
}

}  // namespace blf
