#ifndef BLFL2_CORE_HPP
#define BLFL2_CORE_HPP

#include <Macros.hpp>
#include <Utils.hpp>
#include <interface/Base.hpp>

#include <stddef.h>
#include <stdint.h>

namespace blf {

class Core : public IClockProvider, private INonCopyable {
   public:
    Core() = default;
    virtual ~Core() = default;

    /**
     * Performs CPU initialization.
     */
    virtual void init();
    /**
     * Resets the core.
     * Usually this causes a restart.
     */
    [[noreturn]] virtual void reset() = 0;

    /**
     * Overrides
     */
    virtual uint32_t get_clock_rate() override {
        return 0;
    };
};

class CortexCore : public Core {
   public:
    CortexCore() : Core(){};
    virtual ~CortexCore() = default;

    /**
     * Overrides.
     */
    virtual void init() override;
    [[noreturn]] virtual void reset() override;
    virtual uint32_t get_clock_rate() override;
};

class VirtualCore : public Core {
   public:
    VirtualCore() : Core(){};
    virtual ~VirtualCore() = default;

    /**
     * Overrides.
     */
    virtual void init() override;
    [[noreturn]] virtual void reset() override;
};

}  // namespace blf

#endif /*BLFL2_CORE_HPP*/