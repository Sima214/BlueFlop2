#include "Tracer.hpp"

#include <Macros.hpp>

namespace blf {

void NullTracer::trace(unused Trace& tr) {}
void NullTracer::coredump() {}

}  // namespace blf