#include "Base.hpp"

namespace blf {

IClockProvider* IClockable::set_clock_source(IClockProvider* new_clock_source) {
    IClockProvider* old_clock_source = get_clock_source();
    if (on_clock_source_change(old_clock_source, new_clock_source)) {
        _source = new_clock_source;
        return old_clock_source;
    }
    return nullptr;
}

IClockProvider* IClockable::get_clock_source() {
    return _source;
}

}  // namespace blf