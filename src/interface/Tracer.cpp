#include "Tracer.hpp"

namespace blf {

const char* type2str_common(int type) {
    switch (type) {
        case Trace::NO_MEMORY: return "NO_MEMORY";
        case Trace::UNINITIALIZED: return "UNINITIALIZED";
        case Trace::INTEGER_OVERFLOW: return "INTEGER_OVERFLOW";
        case Trace::STACK_OVERFLOW: return "STACK_OVERFLOW";
        case Trace::BUFFER_OVERFLOW: return "BUFFER_OVERFLOW";
        case Trace::INDEX_OUT_OF_BOUNDS: return "INDEX_OUT_OF_BOUNDS";
        default: return "UNKNOWN";
    }
}

const char* type2str_global(int type) {
    return "UNKNOWN";
}

const char* type2str_libc(int type) {
    return "UNKNOWN";
}

const char* type2str_etl(int type) {
    return "UNKNOWN";
}

const char* type2str_freertos(int type) {
    return "UNKNOWN";
}

const char* type2str_libopencm(int type) {
    return "UNKNOWN";
}

const char* type2str_core(int type) {
    return "UNKNOWN";
}

const char* type2str_board(int type) {
    return "UNKNOWN";
}

const char* type2str_module(int type) {
    return "UNKNOWN";
}

const char* type2str_scheduler(int type) {
    return "UNKNOWN";
}

const char* type2str_communicator(int type) {
    switch (type) {
        case Trace::INVALID_TOKEN: return "INVALID_TOKEN";
        case Trace::ALREADY_PARSED: return "ALREADY_PARSED";
        case Trace::ILL_FORMED: return "ILL_FORMED";
        default: return "UNKNOWN";
    }
}

const char* type2str_linker(int type) {
    return "UNKNOWN";
}

const char* Trace::source2str(TraceSources src) {
    switch (src) {
        case Trace::GLOBAL: return "GLOBAL";
        case Trace::LIBC: return "LIBC";
        case Trace::ETL: return "ETL";
        case Trace::FREERTOS: return "FREERTOS";
        case Trace::LIBOPENCM: return "LIBOPENCM";
        case Trace::CORE: return "CORE";
        case Trace::BOARD: return "BOARD";
        case Trace::MODULE: return "MODULE";
        case Trace::SCHEDULER: return "SCHEDULER";
        case Trace::COMMUNICATOR: return "COMMUNICATOR";
        case Trace::LINKER: return "LINKER";
        default: return "UNKNOWN";
    }
}

const char* Trace::type2str(TraceSources src, int type) {
    if (type < Trace::__COMMON_TYPES_END) {
        return type2str_common(type);
    }
    else {
        switch (src) {
            case Trace::GLOBAL: return type2str_global(type);
            case Trace::LIBC: return type2str_libc(type);
            case Trace::ETL: return type2str_etl(type);
            case Trace::FREERTOS: return type2str_freertos(type);
            case Trace::LIBOPENCM: return type2str_libopencm(type);
            case Trace::CORE: return type2str_core(type);
            case Trace::BOARD: return type2str_board(type);
            case Trace::MODULE: return type2str_module(type);
            case Trace::SCHEDULER: return type2str_scheduler(type);
            case Trace::COMMUNICATOR: return type2str_communicator(type);
            case Trace::LINKER: return type2str_linker(type);
            default: return "UNKNOWN";
        }
    }
}

}  // namespace blf