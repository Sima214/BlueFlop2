#ifndef BLFL2_PORT_HPP
#define BLFL2_PORT_HPP

namespace blf {

class Port {};

class IPort : public Port {};

class OPort : public Port {};

class IOPort : public IPort, public OPort {};

}  // namespace blf

#endif /*BLFL2_PORT_HPP*/