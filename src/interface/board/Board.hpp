#ifndef BLFL2_BOARD_HPP
#define BLFL2_BOARD_HPP

#include <Macros.hpp>
#include <interface/Tracer.hpp>
#include <interface/board/Module.hpp>
#include <interface/board/Port.hpp>
#include <interface/core/Core.hpp>

#include <etl/array_view.h>

namespace blf {

class Board {
   private:
    Core* core;
    Tracer* tracer;
    etl::array_view<Bus*> buses;
    etl::array_view<Peripheral*> peripherals;
    etl::array_view<Port*> ports;

   public:
    Board() = default;
    virtual ~Board() = default;

    /**
     * Getters.
     */
    Core* get_core();
    Tracer& get_tracer();
    etl::array_view<Bus*> get_buses();
    etl::array_view<Peripheral*> get_peripherals();
    etl::array_view<Port*> get_ports();

    /**
     * Used for setting the core once.
     */
    virtual void activate(Core& c);
    /**
     * Override with initialization code.
     */
    virtual void setup() = 0;
    /**
     * One time peripheral setup.
     */
    void setup(Tracer* t, etl::array_view<Bus*>* b, etl::array_view<Peripheral*>* m,
               etl::array_view<Port*>* p);
};

namespace global {

/**
 * Get the Board object.
 *
 * This is a function, because
 * in some systems runtime calculations
 * or operations need to be performed.
 */
Board& get_board(Core& c);

/**
 * Get the Board object.
 *
 * Returns nullptr if the board object
 * has not been initialized.
 */
Board* get_board();

}  // namespace global

}  // namespace blf

#endif /*BLFL2_BOARD_HPP*/