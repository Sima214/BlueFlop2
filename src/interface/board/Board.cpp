#include "Board.hpp"

#include <interface/Tracer.hpp>
#include <interface/board/Module.hpp>

static blf::NullTracer DEFAULT_TRACER;

namespace blf {

Core* Board::get_core() {
    return core;
}

Tracer& Board::get_tracer() {
    if (tracer != nullptr) {
        return *tracer;
    }
    else {
        return DEFAULT_TRACER;
    }
}

etl::array_view<Bus*> Board::get_buses() {
    return buses;
}

etl::array_view<Peripheral*> Board::get_peripherals() {
    return peripherals;
}

etl::array_view<Port*> Board::get_ports() {
    return ports;
}

void Board::activate(Core& c) {
    if (core == nullptr) {
        core = &c;
    }
    else if (core != &c) {
        trap();
    }
}

void Board::setup(Tracer* t, etl::array_view<Bus*>* b, etl::array_view<Peripheral*>* m,
                  etl::array_view<Port*>* p) {
    tracer = t;
    if (b != nullptr) {
        buses = *b;
    }
    if (m != nullptr) {
        peripherals = *m;
    }
    if (p != nullptr) {
        ports = *p;
    }
}

}  // namespace blf