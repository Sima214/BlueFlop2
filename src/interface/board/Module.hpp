#ifndef BLFL2_MODULEBASE_HPP
#define BLFL2_MODULEBASE_HPP

#include <interface/Base.hpp>

namespace blf {

class IModule : public IClockable {};

class Peripheral : public IModule {};

class Bus : public IModule, public IClockDivider {};

class CoreBus : public Bus {};

}  // namespace blf

#endif /*BLFL2_MODULEBASE_HPP*/