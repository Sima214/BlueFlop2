#ifndef BLFL2_VIRTUAL_BOARD_HPP
#define BLFL2_VIRTUAL_BOARD_HPP

#include <interface/board/Board.hpp>

namespace blf {

class VirtualBoard : public Board {
   public:
    VirtualBoard() = default;
    virtual ~VirtualBoard() = default;

    /**
     * Overrides
     */
    virtual void setup() override;
};

}  // namespace blf

#endif /*BLFL2_VIRTUAL_BOARD_HPP*/