#include "Virtual.hpp"

#include <interface/core/Core.hpp>

static blf::VirtualBoard board;
bool board_initialized = false;

namespace blf {

void VirtualBoard::setup() {
    Board::setup(nullptr, nullptr, nullptr, nullptr);
}

namespace global {

Board* get_board() {
    if (board_initialized) {
        return &board;
    }
    else {
        return nullptr;
    }
}

Board& get_board(Core& c) {
    if (!board_initialized) {
        board.setup();
        board.activate(c);
        board_initialized = true;
    }
    return board;
}

}  // namespace global

}  // namespace blf