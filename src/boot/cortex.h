#ifndef BLFL2_CORTEX
#define BLFL2_CORTEX

#include <Macros.hpp>

#include <stddef.h>
#include <stdint.h>

C_DECLS_START

#include <libopencm3/cm3/scb.h>
#include <libopencm3/cm3/vector.h>

// Special types.
typedef struct {
	unsigned int* initial_sp_value; /**< Initial stack pointer value. */
	vector_table_entry_t reset;
	vector_table_entry_t nmi;
	vector_table_entry_t hard_fault;
	vector_table_entry_t memory_manage_fault; /* not in CM0 */
	vector_table_entry_t bus_fault;           /* not in CM0 */
	vector_table_entry_t usage_fault;         /* not in CM0 */
	vector_table_entry_t reserved_x001c[4];
	vector_table_entry_t sv_call;
	vector_table_entry_t debug_monitor;       /* not in CM0 */
	vector_table_entry_t reserved_x0034;
	vector_table_entry_t pend_sv;
	vector_table_entry_t systick;
} small_vector_table_t;

// Forward declarations.
void blueflop2_reset_handler();
void null_handler();

// Linker variables.
typedef void (*funcp_t) (void);
extern size_t _stack;
extern void* __data_start;
extern void* __data_end;
extern void* __data_loadaddr;
extern void* __bss_start;
extern void* __bss_end;
#define __data_size ((size_t) (((uintptr_t) &__data_end) - ((uintptr_t) &__data_start)))
#define __bss_size ((size_t) (((uintptr_t) &__bss_end) - ((uintptr_t) &__bss_start)))
extern funcp_t __preinit_array_start, __preinit_array_end;
extern funcp_t __init_array_start, __init_array_end;
extern funcp_t __fini_array_start, __fini_array_end;

// Vector tables.
__attribute__((__used__,section(".vectors_static")))
small_vector_table_t static_vector_table = {
	.initial_sp_value = &_stack,
	.reset = blueflop2_reset_handler,
	.nmi = null_handler,
	.hard_fault = null_handler,
    .memory_manage_fault = null_handler,
    .bus_fault = null_handler,
    .usage_fault = null_handler,
	.sv_call = null_handler,
    .debug_monitor = null_handler,
	.pend_sv = null_handler,
	.systick = null_handler
};

__attribute__((__used__,section(".vectors_dynamic")))
vector_table_t runtime_vector_table;

C_DECLS_END

#endif /*BLFL2_CORTEX*/