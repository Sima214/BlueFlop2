#include "cortex.h"

#include <Entry.hpp>
#include <Macros.hpp>
#include <interface/board/Board.hpp>
#include <interface/core/Core.hpp>

#include <stdint.h>
#include <string.h>

void blueflop2_reset_handler() {
    /**
     * Ensure 8-byte alignment of stack pointer on interrupts.
     * Enabled by default on most Cortex-M parts, but not M3 r1
     */
    SCB_CCR |= SCB_CCR_STKALIGN;
    // CRT Startup.
    memcpy(&__data_start, &__data_loadaddr, __data_size);
    memset(&__bss_start, 0, __bss_size);
    // Vector table initialization.
    memcpy(&runtime_vector_table, &static_vector_table, sizeof(static_vector_table));
    for (int i = 0; i < NVIC_IRQ_COUNT; i++) {
        runtime_vector_table.irq[i] = null_handler;
    }
    SCB_VTOR = (uintptr_t) &runtime_vector_table;
    // Constructors.
    funcp_t* fp;
    for (fp = &__preinit_array_start; fp < &__preinit_array_end; fp++) {
        (*fp)();
    }
    for (fp = &__init_array_start; fp < &__init_array_end; fp++) {
        (*fp)();
    }
    // Initialize cpu/core.
    blf::CortexCore core((uintptr_t) &_stack, (uintptr_t) &__bss_end);
    core.init();
    core.clockup();
    // Connect board.
    blf::Board* board = blf::global::get_board(core);
    // Call main.
    mainloop(*board);
    // Destructors.
    for (fp = &__fini_array_start; fp < &__fini_array_end; fp++) {
        (*fp)();
    }
    // Reset cpu/core.
    core.reset();
}

void null_handler() {
    nop;
}
