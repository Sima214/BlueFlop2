#include <Entry.hpp>
#include <Macros.hpp>
#include <interface/board/Board.hpp>
#include <interface/core/Core.hpp>

#include <stdint.h>
#include <stdio.h>

int main() {
    // Virtual CPU.
    puts("Preparing CPU.");
    blf::VirtualCore core;
    core.init();
    // Setup board.
    puts("Setting up Board.");
    blf::Board& board = blf::global::get_board(core);
    // Main loop.
    puts("Entering mainloop.");
    mainloop(board);
}