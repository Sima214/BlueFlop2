#include "Serializable.hpp"

#include <Macros.hpp>
#include <communicator/serializer/Serializer.hpp>

#include <stddef.h>
#include <stdint.h>
#include <string.h>

namespace blf {

SerializableProperty* DSerializableRegistryEntry::find_property(uint16_t object_uuid,
                                                                uint16_t prop_uuid) {
    return nullptr;
}

}  // namespace blf