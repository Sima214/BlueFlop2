#include "Serializer.hpp"

#include <Utils.hpp>
#include <interface/Tracer.hpp>

#include <etl/utility.h>
#include <stddef.h>
#include <stdint.h>

namespace blf {

Trace::Compact Serializer::Generator::check_remaining(size_t n) {
    Trace::Compact error;
    if (cold_branch(_buffer == nullptr)) {
        error = Trace::Compact(Trace::COMMUNICATOR, Trace::UNINITIALIZED);
    }
    else if (cold_branch(_remaining < n)) {
        error = Trace::Compact(Trace::COMMUNICATOR, Trace::INDEX_OUT_OF_BOUNDS);
    }
    return error;
}

Trace::Compact Serializer::Generator::write_raw8(uint8_t v) {
    Trace::Compact error = check_remaining(sizeof(v));
    if (!error) {
        uint8_t* buffer = (uint8_t*) _buffer;
        *buffer = v;
        _buffer = (void*) (buffer + 1);
        _remaining -= sizeof(uint8_t);
    }
    return error;
}

Trace::Compact Serializer::Generator::write_raw16(uint16_t v) {
    Trace::Compact error = check_remaining(sizeof(v));
    if (!error) {
        uint16_t* buffer = (uint16_t*) _buffer;
        *buffer = v;
        _buffer = (void*) (buffer + 1);
        _remaining -= sizeof(uint16_t);
    }
    return error;
}

Trace::Compact Serializer::Generator::write_raw24(uint32_t v) {
    int24_t v2(v);
    return write_raw24(v2);
}

Trace::Compact Serializer::Generator::write_raw24(int24_t v) {
    Trace::Compact error = check_remaining(sizeof(v));
    if (!error) {
        int24_t* buffer = (int24_t*) _buffer;
        *buffer = v;
        _buffer = (void*) (buffer + 1);
        _remaining -= sizeof(int24_t);
    }
    return error;
}

Trace::Compact Serializer::Generator::write_raw32(uint32_t v) {
    Trace::Compact error = check_remaining(sizeof(v));
    if (!error) {
        uint32_t* buffer = (uint32_t*) _buffer;
        *buffer = v;
        _buffer = (void*) (buffer + 1);
        _remaining -= sizeof(uint32_t);
    }
    return error;
}

Trace::Compact Serializer::Generator::write_raw64(uint64_t v) {
    Trace::Compact error = check_remaining(sizeof(v));
    if (!error) {
        uint64_t* buffer = (uint64_t*) _buffer;
        *buffer = v;
        _buffer = (void*) (buffer + 1);
        _remaining -= sizeof(uint64_t);
    }
    return error;
}

size_t Serializer::Generator::finalize() {
    if (_buffer != nullptr) {
        size_t used = length - _remaining;
        _buffer = nullptr;
        _remaining = 0;
        return used;
    }
    else {
        return (size_t) -1;
    }
}

Serializer::Generator::~Generator() {
    finalize();
}

Trace::Compact Serializer::Reader::check_remaining(size_t n) {
    Trace::Compact error;
    if (cold_branch(_remaining < n)) {
        error = Trace::Compact(Trace::COMMUNICATOR, Trace::INDEX_OUT_OF_BOUNDS);
    }
    return error;
}

etl::pair<Trace::Compact, uint8_t> Serializer::Reader::read_raw8() {
    etl::pair<Trace::Compact, uint8_t> r;
    Trace::Compact error = check_remaining(sizeof(uint8_t));
    if (!error) {
        uint8_t* buffer = (uint8_t*) _pos;
        r.second = *buffer;
        _pos = (void*) (buffer + 1);
        _remaining -= sizeof(uint8_t);
    }
    r.first = error;
    return r;
}

etl::pair<Trace::Compact, uint16_t> Serializer::Reader::read_raw16() {
    etl::pair<Trace::Compact, uint16_t> r;
    Trace::Compact error = check_remaining(sizeof(uint16_t));
    if (!error) {
        uint16_t* buffer = (uint16_t*) _pos;
        r.second = *buffer;
        _pos = (void*) (buffer + 1);
        _remaining -= sizeof(uint16_t);
    }
    r.first = error;
    return r;
}

etl::pair<Trace::Compact, uint32_t> Serializer::Reader::read_raw24e() {
    etl::pair<Trace::Compact, uint32_t> r;
    auto v = read_raw24();
    r.first = v.first;
    r.second = (int) v.second;
    return r;
}

etl::pair<Trace::Compact, int24_t> Serializer::Reader::read_raw24() {
    etl::pair<Trace::Compact, int24_t> r;
    Trace::Compact error = check_remaining(sizeof(int24_t));
    if (!error) {
        int24_t* buffer = (int24_t*) _pos;
        r.second = *buffer;
        _pos = (void*) (buffer + 1);
        _remaining -= sizeof(int24_t);
    }
    r.first = error;
    return r;
}

etl::pair<Trace::Compact, uint32_t> Serializer::Reader::read_raw32() {
    etl::pair<Trace::Compact, uint32_t> r;
    Trace::Compact error = check_remaining(sizeof(uint32_t));
    if (!error) {
        uint32_t* buffer = (uint32_t*) _pos;
        r.second = *buffer;
        _pos = (void*) (buffer + 1);
        _remaining -= sizeof(uint32_t);
    }
    r.first = error;
    return r;
}

etl::pair<Trace::Compact, uint64_t> Serializer::Reader::read_raw64() {
    etl::pair<Trace::Compact, uint64_t> r;
    Trace::Compact error = check_remaining(sizeof(uint64_t));
    if (!error) {
        uint64_t* buffer = (uint64_t*) _pos;
        r.second = *buffer;
        _pos = (void*) (buffer + 1);
        _remaining -= sizeof(uint64_t);
    }
    r.first = error;
    return r;
}

}  // namespace blf