#ifndef BLFL2_SERIALIZER_HPP
#define BLFL2_SERIALIZER_HPP

#include <Utils.hpp>
#include <interface/Tracer.hpp>

#include <etl/utility.h>
#include <stddef.h>
#include <stdint.h>

namespace blf {

class SerializableProperty;

class Serializer {

   public:
    class Spec {
       protected:
        /**
         * Size of handle in bytes.
         */
        const size_t handle_size;

       public:
        Spec(size_t handle_size) : handle_size(handle_size) {}
        size_t get_handle_size() const {
            return handle_size;
        }
    };

    class Generator : public INonCopyable {
       protected:
        const Spec& specs;
        const size_t length;
        void* _buffer;
        size_t _remaining;

       public:
        /**
         * Create a new Generator and bind it to an
         * already allocated and zeroed memory area.
         */
        Generator(const Spec& specs, void* buffer, size_t size) :
            specs(specs), length(size), _buffer(buffer), _remaining(size) {}
        /**
         * Finalizes format and returns written bytes.
         */
        size_t finalize();
        ~Generator();

        Trace::Compact check_remaining(size_t n);
        Trace::Compact write_raw8(uint8_t v);
        Trace::Compact write_raw16(uint16_t v);
        Trace::Compact write_raw24(uint32_t b);
        Trace::Compact write_raw24(int24_t b);
        Trace::Compact write_raw32(uint32_t b);
        Trace::Compact write_raw64(uint64_t b);
    };

    class Reader {
       protected:
        const Spec* specs;
        void* _pos;
        size_t _remaining;

       public:
        nonnull(2, 3) Reader(const Spec* specs, void* buffer, size_t size) :
            specs(specs), _pos(buffer), _remaining(size) {}
        ~Reader() = default;

        Trace::Compact check_remaining(size_t n);
        etl::pair<Trace::Compact, uint8_t> read_raw8();
        etl::pair<Trace::Compact, uint16_t> read_raw16();
        etl::pair<Trace::Compact, uint32_t> read_raw24e();
        etl::pair<Trace::Compact, int24_t> read_raw24();
        etl::pair<Trace::Compact, uint32_t> read_raw32();
        etl::pair<Trace::Compact, uint64_t> read_raw64();

        void* get_pos() {
            return _pos;
        }

        bool buffer_end() {
            return _remaining == 0;
        }
    };

    /**
     * Serializer is just a class container.
     */
    Serializer() = delete;
    ~Serializer() = delete;

};

}  // namespace blf

#endif /*BLFL2_SERIALIZER_HPP*/