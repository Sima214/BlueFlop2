#ifndef BLFL2_SERIALIZABLE_HPP
#define BLFL2_SERIALIZABLE_HPP

#include <Utils.hpp>
#include <communicator/serializer/Serializer.hpp>
#include <interface/Tracer.hpp>

#include <etl/array_view.h>
#include <etl/bitset.h>
#include <stddef.h>
#include <stdint.h>

namespace blf {

class SerializableProperty : public INonCopyable {
   private:
    const char* name;
    const uint16_t parent_object_uuid;
    const uint16_t prop_uuid;
    const bool perm_read;
    const bool perm_write;
    const bool perm_exec;
    /**
     * [-1, 4095]
     */
    const int16_t size;

   public:
    constexpr SerializableProperty(const char* name, uint16_t parent_object_uuid,
                                   uint16_t prop_uuid, bool perm_read, bool perm_write,
                                   bool perm_exec, int size) :
        name(name),
        parent_object_uuid(parent_object_uuid), prop_uuid(prop_uuid), perm_read(perm_read),
        perm_write(perm_write), perm_exec(perm_exec), size(clamp(size, -1, 4095)) {}
    virtual ~SerializableProperty() = default;

    /**
     * Getters
     */
    int16_t get_data_size();
};

class DSerializableRegistryEntry : public INonCopyable {
   protected:
    const char* name;
    const uint16_t object_uuid;
    const size_t properties_count;
    etl::array_view<SerializableProperty*> properties;
    const size_t inherited_count;
    etl::array_view<DSerializableRegistryEntry*> inherited;

   public:
    DSerializableRegistryEntry(const char* name, uint16_t object_uuid, size_t properties_count,
                               size_t inherited_count) :
        name(name),
        object_uuid(object_uuid), properties_count(properties_count),
        inherited_count(inherited_count) {}
    virtual ~DSerializableRegistryEntry() = default;
    /**
     * Finds the property object.
     *
     * Uses BFS to search inherited classes.
     */
    virtual SerializableProperty* find_property(uint16_t object_uuid, uint16_t prop_uuid) = 0;
    /**
     * Overrides
     */

   protected:
    SerializableProperty* _find_property(uint16_t object_uuid, uint16_t prop_uuid,
                                         etl::ibitset& visited_entries);
};

/**
 * PROPERTIES_N: number of new and overriden properties.
 * INHERIT_N: number of inherited serializable classes.
 */
template<size_t PROPERTIES_N, size_t INHERIT_N>
class SerializableRegistryEntry : public DSerializableRegistryEntry {
   public:
    SerializableRegistryEntry(const char* name, uint16_t object_uuid,
                              const DSerializableRegistryEntry* inherits[INHERIT_N],
                              const SerializableProperty* props[PROPERTIES_N]) :
        DSerializableRegistryEntry(name, object_uuid, PROPERTIES_N, INHERIT_N) {
        inherited = inherits;
        properties = props;
    }
    virtual ~SerializableRegistryEntry() = default;
};

template<size_t N> class SerializableRegistry : public INonCopyable {
   private:
    uint16_t next_object_uuid = 1;

   public:
    SerializableRegistry();
};

class ISerializableObject {
   protected:
    const DSerializableRegistryEntry& serializable_object;

   public:
    ISerializableObject(DSerializableRegistryEntry& so) : serializable_object(so) {}
    virtual ~ISerializableObject() = default;
    Trace::Compact on_property_exec(DSerializableRegistryEntry& object,
                                    SerializableProperty& property, void* data);
    Trace::Compact on_property_write(DSerializableRegistryEntry& object,
                                     SerializableProperty& property, void* data);
    Trace::Compact on_property_read(DSerializableRegistryEntry& object,
                                    SerializableProperty& property, void* data);
};

}  // namespace blf

#endif /*BLFL2_SERIALIZABLE_HPP*/