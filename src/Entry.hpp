#ifndef BLFL2_ENTRY
#define BLFL2_ENTRY

#include <interface/board/Board.hpp>

/**
 * Called after the CPU/CORE gets initialized and clocked.
 * Starts the communicator and scheduler.
 */
void mainloop(blf::Board& core);

#endif /*BLFL2_ENTRY*/
